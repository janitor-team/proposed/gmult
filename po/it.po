# Italian messages for gmult.
# Copyright (C) 2005, 2008, Free Software Foundation, Inc.
# This file is distributed under the same license as the gmult package.
# Marco Colombo <m.colombo@ed.ac.uk>, 2005, 2008.
#
msgid ""
msgstr ""
"Project-Id-Version: gmult 6.0\n"
"Report-Msgid-Bugs-To: mike@mterry.name\n"
"POT-Creation-Date: 2011-04-13 08:03-0400\n"
"PO-Revision-Date: 2010-04-05 22:33+0000\n"
"Last-Translator: Michael Terry <michael.terry@canonical.com>\n"
"Language-Team: Italian <tp@lists.linux.it>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2011-04-13 13:36+0000\n"
"X-Generator: Launchpad (build 12792)\n"

#. Translators: If there's a better alternative to Arabic numerals for
#. your language, please use it instead of these digits.  Note
#. that it still has to make sense when used as part of the math puzzle.
#: ../gmult/DigitBox.vala:48 ../gmult/GtkMult.vala:99
msgid "0"
msgstr "0"

#: ../gmult/DigitBox.vala:49 ../gmult/GtkMult.vala:100
msgid "1"
msgstr "1"

#: ../gmult/DigitBox.vala:50 ../gmult/GtkMult.vala:101
msgid "2"
msgstr "2"

#: ../gmult/DigitBox.vala:51 ../gmult/GtkMult.vala:102
msgid "3"
msgstr "3"

#: ../gmult/DigitBox.vala:52 ../gmult/GtkMult.vala:103
msgid "4"
msgstr "4"

#: ../gmult/DigitBox.vala:53 ../gmult/GtkMult.vala:104
msgid "5"
msgstr "5"

#: ../gmult/DigitBox.vala:54 ../gmult/GtkMult.vala:105
msgid "6"
msgstr "6"

#: ../gmult/DigitBox.vala:55 ../gmult/GtkMult.vala:106
msgid "7"
msgstr "7"

#: ../gmult/DigitBox.vala:56 ../gmult/GtkMult.vala:107
msgid "8"
msgstr "8"

#: ../gmult/DigitBox.vala:57 ../gmult/GtkMult.vala:108
msgid "9"
msgstr "9"

#. to open display ('cause we passed false above)
#: ../gmult/GtkMult.vala:82 ../gmult/GtkMult.vala:515
#: ../data/gmult.desktop.in.h:2
msgid "Multiplication Puzzle"
msgstr "Moltiplicazione Cifrata"

#. Translators: The next several characters are used to show the 'unknown'
#. digits in the puzzle.  Translate to something that makes sense for
#. your alphabet.
#: ../gmult/GtkMult.vala:89 ../gmult/TableBox.vala:54
msgid "A"
msgstr "A"

#: ../gmult/GtkMult.vala:90 ../gmult/TableBox.vala:55
msgid "B"
msgstr "B"

#: ../gmult/GtkMult.vala:91 ../gmult/TableBox.vala:56
msgid "C"
msgstr "C"

#: ../gmult/GtkMult.vala:92 ../gmult/TableBox.vala:57
msgid "D"
msgstr "D"

#: ../gmult/GtkMult.vala:93 ../gmult/TableBox.vala:58
msgid "E"
msgstr "E"

#: ../gmult/GtkMult.vala:94 ../gmult/TableBox.vala:59
msgid "F"
msgstr "F"

#: ../gmult/GtkMult.vala:95 ../gmult/TableBox.vala:60
msgid "G"
msgstr "G"

#: ../gmult/GtkMult.vala:96 ../gmult/TableBox.vala:61
msgid "H"
msgstr "H"

#: ../gmult/GtkMult.vala:97 ../gmult/TableBox.vala:62
msgid "I"
msgstr "I"

# NdT: tanto per avere solo lettere dell'alfabeto italiano
#: ../gmult/GtkMult.vala:98 ../gmult/TableBox.vala:63
msgid "J"
msgstr "L"

#: ../gmult/GtkMult.vala:157
msgid "_Game"
msgstr "_Gioco"

#: ../gmult/GtkMult.vala:164
msgid "_Hint"
msgstr "_Suggerimento"

#: ../gmult/GtkMult.vala:169
msgid "_Solve"
msgstr "_Risolvi"

#: ../gmult/GtkMult.vala:178
msgid "_Help"
msgstr "A_iuto"

#: ../gmult/GtkMult.vala:295
msgid "Congratulations!"
msgstr "Complimenti!"

#. Translators: First argument is letter, second is digit
#: ../gmult/GtkMult.vala:305
#, c-format
msgid "Incorrect — %1$s is not %2$s"
msgstr "Sbagliato: %1$s non è %2$s"

#. Translators: First argument is letter, second is digit
#: ../gmult/GtkMult.vala:309
#, c-format
msgid "Correct — %1$s is %2$s"
msgstr "Corretto: %1$s è %2$s"

#: ../gmult/GtkMult.vala:345
#, c-format
msgid "Incorrect Guesses"
msgstr "Non hai indovinato"

#: ../gmult/GtkMult.vala:356
#, c-format
msgid "Time"
msgstr "Orario"

#: ../gmult/GtkMult.vala:376
msgid "Close Multiplication Puzzle"
msgstr "Esci da Moltiplicazione Cifrata"

#: ../gmult/GtkMult.vala:388
msgid "Start a new game"
msgstr "Inizia un nuovo gioco"

#: ../gmult/GtkMult.vala:399
msgid "Puzzle solved"
msgstr "Rompicapo risolto"

#: ../gmult/GtkMult.vala:408
msgid "Solve this game"
msgstr "Risolvi questo gioco"

#. Translators: First argument is letter, second is digit
#: ../gmult/GtkMult.vala:442
#, c-format
msgid "%1$s is %2$s"
msgstr "%1$s è %2$s"

#: ../gmult/GtkMult.vala:455
msgid "Reveal a digit at random"
msgstr "Scopri una cifra a  caso"

#: ../gmult/GtkMult.vala:475 ../gmult/GtkMult.vala:495
msgid "About Multiplication Puzzle"
msgstr "Informazioni su Moltiplicazione Cifrata"

#: ../gmult/GtkMult.vala:478
msgid "translator-credits"
msgstr ""
"Marco Colombo <m.colombo@ed.ac.uk>\n"
"\n"
"Launchpad Contributions:\n"
"  LoReNicolò https://launchpad.net/~god121-p-l\n"
"  Michael Terry https://launchpad.net/~mterry\n"
"  bertu https://launchpad.net/~albi-mana"

#: ../data/gmult.desktop.in.h:1
msgid "Figure out which letters are which numbers"
msgstr "Indovina quale numero corrisponde a ogni lettera"

# FIXME
#, c-format
#~ msgid "Could not display %s"
#~ msgstr "Impossibile mostrare %s"

#, c-format
#~ msgid ""
#~ "This program is free software; you can redistribute it and/or modify it "
#~ "under the terms of the GNU General Public License as published by the Free "
#~ "Software Foundation; either version 3 of the License, or (at your option) "
#~ "any later version."
#~ msgstr ""
#~ "Questo è software libero; potete redistribuirlo e/o modificarlo secondo i "
#~ "termini della GNU General Public Licence come pubblicata dalla Free Software "
#~ "Foundation; la versione 3 della Licenza o (a vostra scelta) ogni versione "
#~ "successiva."

#~ msgid ""
#~ "This program is distributed in the hope that it will be useful, but WITHOUT "
#~ "ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or "
#~ "FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for "
#~ "more details."
#~ msgstr ""
#~ "Questo programma è distribuito con la speranza che possa essere utile, ma "
#~ "SENZA ALCUNA GARANZIA; anche senza la garanzia implicita di COMMERCIABILITÀ "
#~ "o di IDONEITÀ AD UNO SCOPO PARTICOLARE. Si consulti la GNU General Public "
#~ "License per ulteriori dettagli."

#~ msgid ""
#~ "You should have received a copy of the GNU General Public License along with "
#~ "this program; if not, write to the Free Software Foundation, Inc., 59 Temple "
#~ "Place - Suite 330, Boston, MA  02111-1307, USA."
#~ msgstr ""
#~ "Dovreste aver ricevuto una copia della GNU General Public License con questo "
#~ "programma; altrimenti scrivere a Free Software Foundation, Inc., 59 Temple "
#~ "Place - Suite 330, Boston, MA  02111-1307, USA."
