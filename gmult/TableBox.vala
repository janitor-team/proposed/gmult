/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*- */
/*
    Multiplication Puzzle
    Copyright (C) 2004-2008,2011 Michael Terry <mike@mterry.name>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

using GLib;

public class TableBox : CharBox
{
  public Canvas canvas { get; construct; }
  public char character { get; construct; }
  
  public TableBox (char c, Canvas can)
  {
    Object(character: c, canvas: can);
  }
  
  construct
  {
    did_enter = false;
    drag_status = true;
    
    this.letter = "%c".printf (character);
    this.border = Gtk.ShadowType.IN;
    
    Gtk.TargetEntry[] entries = new Gtk.TargetEntry[1];
    entries[0].target = "STRING";
    entries[0].flags = Gtk.TargetFlags.SAME_APP;
    entries[0].info = 0;
    Gtk.drag_dest_set (this, 0, entries, Gdk.DragAction.MOVE);
    
    drag_data_received.connect(handle_data_received);
    drag_motion.connect(handle_drag_motion);
    drag_leave.connect(handle_drag_leave);
    drag_drop.connect(handle_drag_drop);

    // Translators: The next several characters are used to show the 'unknown'
    // digits in the puzzle.  Translate to something that makes sense for
    // your alphabet.
    N_("A");
    N_("B");
    N_("C");
    N_("D");
    N_("E");
    N_("F");
    N_("G");
    N_("H");
    N_("I");
    N_("J");
  }
  
  private bool did_enter;
  private bool drag_status;
  private bool handle_drag_motion (Gdk.DragContext context,
                                    int x, int y, uint time_)
  {
    if (!did_enter) {
      canvas.highlight_box (this, false, true, true);
      did_enter = true;
    }
    
    drag_status = false;
    Gtk.drag_get_data (this, context, Gdk.Atom.intern_static_string ("STRING"), time_);
    
    if (!drag_status)
      canvas.highlight_box (this, false, true, true);
    
    return drag_status;
  }
  
  private void handle_drag_leave (Gdk.DragContext context,
                                   uint time_)
  {
    canvas.highlight_box (this, false, false, true);
    did_enter = false;
  }
  
  private bool handle_drag_drop (Gdk.DragContext context,
                                  int x, int y, uint time_)
  {
    drag_status = true;
    Gtk.drag_get_data (this, context, Gdk.Atom.intern_static_string ("STRING"), time_);
    
    if (!drag_status)
      canvas.highlight_box (this, false, true, true);
    
    return true;
  }
  
  private void handle_data_received (Gdk.DragContext context,
                                      int x, int y, Gtk.SelectionData data,
                                      uint info, uint time_)
  {
    if (data.get_length() <= 0)
      return;
    
    int digit = data.get_data()[0] - 48;
    
    if (!drag_status)
    {
      bool[] guesses = canvas.puzzle.get_letter_guesses ((MultPuzzleChar)character);
      
      if (!guesses[digit])
      {
        drag_status = true;
        Gdk.drag_status (context, Gdk.DragAction.COPY, time_);
      }
    }
    else
    {
      var rv = canvas.puzzle.guess (digit, (MultPuzzleChar)character);
      Gtk.drag_finish (context, (rv == MultPuzzleGuessStatus.CORRECT), false, time_);
    }
  }
}

